<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="../pastebin.css" type="text/css" />
</head>
<body>
<?

	include('geshi/geshi.php');
	
	$pasteFile = "pastes/" . $_GET[paste] . ".txt";
	if(file_exists($pasteFile)){
		$file = fopen($pasteFile,'rb') or die($pasteFile.' not found');
		
		$pasteContent = "";
		while(!feof($file)){  
			$pasteContent .= fread($file,2048); 
		}
		
		$language = 'php';
		$path = 'geshi/';	
		
		$geshi = new GeSHi($pasteContent, $language, $path);
		$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS); 
		$geshi->get_stylesheet();
	
		echo $geshi->parse_code();
	}else{
		echo "Sorry. The paste doesn't exist";
	}
	
?>
</body>
</html>
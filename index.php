<?php
if(isset($_POST['pastecontent'])){ //User has submitted a new paste.
  //Let's check the latest inserted paste ID.
  $fp = fopen('private/currid.txt','rwb+') or die('fopen');
  for($i=0;!flock($fp,LOCK_EX)&&$i<100;$i++){ //Do not allow multiple checks at same time.
    usleep(50);
  }

  $currId = fread($fp,2048) or die('fread');
  $pasteId = $currId + 1;
  ftruncate($fp,0) or die('ftruncate');
  fseek($fp,0) and die('fseek');
  fwrite($fp, $pasteId) or die('fwrite');
  fclose($fp) or die('fclose');

  //Well.. now we just have to save the paste :o)
  $pasteFiletype = $_POST['pastefiletype'];
  $allowedFiletypes = array('php','html','asp','txt');
  if (!in_array($pasteFiletype,$allowedFiletypes)){
    $pasteFiletype = 'txt';
  }
  switch ($_POST['expire']){
  case 'day':
    $pasteExpire = strtotime("now +1 day");
    break;
  case 'month':
    $pasteExpire = strtotime("now +1 month");
    break;
  default:
    $pasteExpire = 0;
  }
  $prev = '0';
  if (isset($_POST['prev']) && is_numeric($_POST['prev'])){
    $prev = $_POST['prev'];
  }

  $pasteName = $pasteId.'_'.$pasteFiletype.'_'.$pasteExpire.'_'.$prev; //FINISH THIS
  if (file_exists('pastes/'.$pasteName)){
    die('error');
  }
  $fp = fopen('pastes/'.$pasteName,'wb') or die('error fopen');
  fwrite($fp,$_POST['pastecontent']) or die('error fwrite');
  fclose($fp) or die('fclose');
?>
<div><?php echo $_POST['pastecontent']; ?></div>
<textarea rows=6 cols=30><?php echo $_POST['pastecontent']; ?></textarea>
<?php
  die();
}
?>
<form action="" method="post">
	<select name="pastefiletype">
		<option value="plain">Plain Text</option>
		<option value="php" selected="selected">PHP</option>
	</select><br />
	<textarea name="pastecontent" style="width:400px;height:200px;"></textarea><br />
	I want my paste to expire after 
	<input type="radio" name="expire" value="day" id="day"> <label for="day">a day</label>
	<input type="radio" name="expire" value="month" id="month"> <label for="month">a month</label>
	<input type="radio" name="expire" value="never" id="never" checked="checked"> <label for="never">never</label>
	<br />
	<input type="submit" value="Paste me!" />
</form>

<?php
function tohex($data){
  $ret = array();
  $offset = 0;
  $len = strlen($data);

  $ascii = '';
  $hex = array();
  for ($i = $j = 0; $i < $len; $i++){
    $hex[] = sprintf("%02x", ord($data[$i]));

    if ((ord($data[$i]) >= ord(' ') && ord($data[$i]) <= ord('~')) || (ord($data[$i]) >= 0xa0 && ord($data[$i]) <= 0xa7)){
      $ascii .= utf8_encode(htmlentities($data[$i]));
    } else {
      $ascii .= '.';
    }

    if ($j == 7) $ascii .= ' ';

    if (++$j == 16 || $i == $len - 1){
      $ret[] = array(sprintf("%04x",$offset),$hex,$ascii);

      $ascii = '';
      $hex = array();

      $offset += 16;
      $j = 0;
    }
  }
  return $ret;
}
?>
